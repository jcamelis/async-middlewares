'use strict';

var q = require("q");
var _ = require("lodash");

function call(fn) {
	return _.isFunction(fn) && fn();
}

function runNext(req, res, next) {
	call(next);
}

function getMiddlewares(_package, methods) {
	var middlewares = [];
	_.forEach(methods, function name(key) {
		if (_.isFunction(_package[key])) {
			middlewares.push(_package[key]);
		} else {
			console.log("method: " + key + " no es de tipo function");
		}
	});
	return middlewares || _package;
}

module.exports = function(config) {

	var configMiddlewares = config["async-middlewares"];
	
	return function (req, res, next) {

		function processConfig(_middlewaresConfig, _dependency) {
			var promises = [];
			_.forEach(_middlewaresConfig, function (_middlewareConfig, _middlewareName) {
				var promise = middlewareFactory(_middlewareConfig, _middlewareName, _dependency);
	
				promises.push(promise);
			});
			return promises;
		}

		function middlewareFactory(middlewareConfig, middlewareName, promise) {


			var _package = require(middlewareName)(config);

			var deferred = q.defer();
			
			var _promises = [];
			/**
			 * @var middlewares {array}
			 */
			var middlewares;
			
			/**
			 * Obtengo el/los middleware a ejecutar
			 */
			if (_.isString(middlewareConfig)) {

				middlewares = getMiddlewares(_package, [middlewareConfig]);

			} else if (!!middlewareConfig.method && _.isString(middlewareConfig.method)) {

				middlewares = getMiddlewares(_package, [middlewareConfig.method]);

			} else if (_.isArray(middlewareConfig)) {
				middlewares = getMiddlewares(_package, middlewareConfig);
			} else if (_.isFunction(_package)) {
				middlewares = [_package];
			} else {
				console.log(middlewareName + " no es de tipo function");
			}
				
			if (_.isObject(middlewareConfig)) {

				if (middlewareConfig.then && _.isObject(middlewareConfig.then)) {
					 _promises = processConfig(middlewareConfig.then, deferred.promise);
				}
			}
			/**
			 * 
			 */
			function callMiddlewares() {
				_.forEach(middlewares, function (md) {
					(md || runNext)(req, res, deferred.resolve);
				});
			}

			if (q.isPromise(promise)) {
				promise.then(callMiddlewares);
			} else {
				callMiddlewares();
			};

			return _promises.length ? q.all(_promises) : deferred.promise;
		}
		/**
		 * 
		 */
		var promises = processConfig(configMiddlewares);

		q.all(promises).then(next, next);
	};
};