'use strict';

var _ = require("lodash");

module.exports = function (config) {
	config = _.merge({}, config);
	
	return require("./async-middlewares")(config);
};